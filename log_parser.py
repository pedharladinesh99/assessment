from collections import defaultdict
import ipaddress


def parse_logs(path, subnets_list):
    ips = defaultdict(int)
    subnets = defaultdict(int)
    with open(path) as f:
        for line in f.read().splitlines():
            values = line.split('-')
            ip = values[0].strip()
            ips[ip] = ips[ip] + 1
    for ip in ips:
        print("Address {ip} was encountered {n} time(s)".format(ip=ip, n=ips[ip]))
    print('--------------------------------------------------')
    for subnet in subnets_list:
        for ip in ips:
            try:
                if ip_in_prefix(ip, subnet):
                    subnets[subnet] += ips[ip]
            except Exception as e:
                pass
        print("The bucket {0} contains {1} addresses".format(subnet, subnets[subnet]))
    return {
        'buckets': subnets,
        'addresses': ips
    }


def ip_in_prefix(ip_address, prefix):
    ip = ipaddress.ip_address(ip_address)
    if type(ip) is ipaddress.IPv4Address:
        return ip in ipaddress.IPv4Network(prefix)
    else:
        return ip in ipaddress.IPv6Network(prefix)


def parse():
    subnet_config = 'subnets.txt'
    subnets = []
    with open(subnet_config) as f:
        subnets = [s.strip() for s in f.read().splitlines()]
    return parse_logs('nginx.log', subnets)


if __name__ == '__main__':
	parse()