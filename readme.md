# SRE Take Home Assessment

In this assessment, I used flask framework to design lite-weight web-app to serve our api calls.

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install flask.

```bash
pip install flask
```
I used docker to run the app. install docker and run the following commands to start the application.

## Assumptions
  - I have assumed nginx.log is passed as path to the script.
  - I assumed subnets are passed from config file which contains subnet details.


## Usage
#### Dockerfile
```dockerfile
FROM python:alpine3.7
# copy all files to dir app
COPY . /app

# change working dir to /app
WORKDIR /app

# install required python packages
RUN pip install -r requirements.txt

# expose port 8080
EXPOSE 8080

# set extrypoint
ENTRYPOINT [ "python" ]
CMD [ "app.py" ]
```

#### Instructions to run the app using docker
```bash
# This will create image the name: test and tag: latest
docker build -t test:latest .

# run the container
# -p 8080:8080 expose the port 8080 outside container
docker run -p 8080:8080 test:latest
```

#### Instructions to run the flask app using python
```bash
python app.py
```
## Output

#### Api response json on calling http://localhost:8080
  - when below subnets are given:
```txt
108.162.0.0/16
212.129.32.128/25
173.245.56.0/23
2001:19f0:5c01:03ca::/64
```
#### response json
```json
{
  "buckets": {
    "108.162.0.0/16": 40,
    "212.129.32.128/25": 8,
    "173.245.56.0/23": 23,
    "2001:19f0:5c01:03ca::/64": 8
  },
  "addresses": {
    "88.198.21.48": 24,
    "163.172.71.138": 16,
    "2001:41d0:c:ec9::2b43:1d3": 20,
    "163.172.185.23": 16,
    "5.9.147.226": 16,
    "5.196.4.135": 24,
    "217.182.198.104": 4,
    "91.121.119.225": 16,
    ...
    ...
  }
}
```

#### logs in flask app
```
Address 88.198.21.48 was encountered 24 time(s)
Address 163.172.71.138 was encountered 16 time(s)
Address 2001:41d0:c:ec9::2b43:1d3 was encountered 20 time(s)
Address 163.172.185.23 was encountered 16 time(s)
Address 5.9.147.226 was encountered 16 time(s)
Address 5.196.4.135 was encountered 24 time(s)
Address 217.182.198.104 was encountered 4 time(s)
Address 91.121.119.225 was encountered 16 time(s)
Address 2a01:4f8:10a:f229::1 was encountered 20 time(s)
Address 2001:bc8:4400:2400::4917 was encountered 28 time(s)
Address 2a01:4f8:151:5307:2::211 was encountered 8 time(s)
...
...
...
Address 173.245.50.230 was encountered 1 time(s)
Address 173.245.56.221 was encountered 6 time(s)
Address 173.245.54.223 was encountered 1 time(s)
Address 108.162.218.17 was encountered 1 time(s)
Address 108.162.219.38 was encountered 2 time(s)
Address 108.162.219.32 was encountered 1 time(s)
--------------------------------------------------
The bucket 108.162.0.0/16 contains 40 addresses
The bucket 212.129.32.128/25 contains 8 addresses
The bucket 173.245.56.0/23 contains 23 addresses
The bucket 2001:19f0:5c01:03ca::/64 contains 8 addresses
```
