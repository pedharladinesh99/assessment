from flask import Flask
import json
import log_parser

app = Flask(__name__)

@app.route('/')
def parse():
    try:
        data = log_parser.parse()
    except Exception as e:
        print(str(e))
        data = {}
    response = app.response_class(
        response=json.dumps(data),
        status=200,
        mimetype='application/json'
    )
    return response

# main driver function
if __name__ == '__main__':
	app.run(host ='0.0.0.0', port = 8080)
